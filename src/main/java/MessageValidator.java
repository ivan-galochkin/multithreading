import com.google.gson.JsonObject;
import lombok.Setter;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

@Setter
class MessageValidator {
    CopyOnWriteArraySet<Message> validMessages;
    CopyOnWriteArraySet<Message> invalidMessages;
    ConcurrentHashMap<String, JsonObject> enrichmentData;

    public JsonObject validate(Message message) throws BadMessage {
        return null;
    }
}
