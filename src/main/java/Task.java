import java.util.concurrent.Callable;

class Task implements Callable {
    Message message;
    EnrichmentService enrichmentService;

    Task(Message message, EnrichmentService service) {
        this.message = message;
        this.enrichmentService = service;
    }

    @Override
    public Object call() {
        System.out.println(enrichmentService.enrich(message));
        return null;
    }
}
