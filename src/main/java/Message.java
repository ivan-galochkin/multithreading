class Message {
    String content;
    EnrichmentType enrichmentType;

    enum EnrichmentType {
        TYPE,
        MSISDN,
    }

    Message(String content, EnrichmentType type) {
        this.content = content;
        this.enrichmentType = type;
    }
}
