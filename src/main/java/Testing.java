import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

class Testing {
    EnrichmentService service;
    List<Callable<String>> callableTasks;

    Testing(EnrichmentService service) {
        this.service = service;
        this.callableTasks = new ArrayList<>();
    }

    void addTest(Callable<String> task) {
        this.callableTasks.add(task);
    }

    public void test() {
        var executorService = Executors.newFixedThreadPool(2);
        try {
            executorService.invokeAll(callableTasks);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
