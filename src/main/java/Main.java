import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

class Main {
    public static void main(String[] args) {
        var validator = new MsisdnMessageValidator();
        var map = new ConcurrentHashMap<String, JsonObject>();

        for (int i = 5; i >= 0; i--) {
            map.put(String.format("8800555353%o", i), new Gson().
                    fromJson(String.format("{ \"temp\": \"123\",   \"idofdata\": \"%o\" }", i), JsonObject.class));
        }

        validator.setEnrichmentData(map);
        validator.setValidMessages(new CopyOnWriteArraySet<>());
        validator.setInvalidMessages(new CopyOnWriteArraySet<>());
        var service = new EnrichmentService(validator);
        var testing = new Testing(service);
        for (int i = 5; i >= 0; i--) {
            testing.addTest(new Task(
                    new Message(
                            String.format("{   \"msisdn\": \"8800555353%o\",   \"someinf\": \"123\" }", i),
                            Message.EnrichmentType.MSISDN), service));
        }
        // отсутствие msisdn
        testing.addTest(new Task(new Message("{\"someinf\": \"321\"}", Message.EnrichmentType.MSISDN), service));
        // неправильный тип enrichment
        testing.addTest(new Task(new Message("{   \"msisdn\": \"88005553535\",   \"someinf\": \"321\" }", Message.EnrichmentType.TYPE), service));

        testing.test();
    }
}