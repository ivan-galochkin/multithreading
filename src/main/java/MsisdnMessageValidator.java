import com.google.gson.Gson;
import com.google.gson.JsonObject;

class MsisdnMessageValidator extends MessageValidator {
    @Override
    public JsonObject validate(Message message) throws BadMessage {
        try {
            if (message.enrichmentType != Message.EnrichmentType.MSISDN) {
                throw new BadMessage();
            }

            var msisdnObj = new Gson().fromJson(message.content, MsisdnContentSchema.class);
            var obj = new Gson().fromJson(message.content, JsonObject.class);
            var data = this.enrichmentData.get(msisdnObj.msisdn);
            validMessages.add(message);
            obj.add("enrichment", data);
            return obj;
        } catch (Exception e) {
            invalidMessages.add(message);
            throw new BadMessage();
        }
    }
}
