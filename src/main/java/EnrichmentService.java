import com.google.gson.Gson;

class EnrichmentService {
    private final MessageValidator validator;

    public EnrichmentService(MessageValidator validator) {
        this.validator = validator;
    }

    public String enrich(Message message) {
        try {
            return new Gson().toJson(validator.validate(message));
        } catch (BadMessage e) {
            return "Not valid message";
        }
    }
}
